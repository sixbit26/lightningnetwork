#include <vector>
#include <math.h>
#include<iomanip>
#include<iostream>
#include<stdlib.h>
#include <random>
#include <map>
#include <chrono>
#include <fstream>
#include <cstdio>
#include <sstream>

using namespace std;
double casual();
int **malloc2d_int(int , int );
double **malloc2d_double(int , int );
void coloring(int, int, int, int*, vector<int>[]);
int sign_fun(double );
double randMToN(double, double);
int main(){
    srand(time(NULL));
    int N=5e4;
    vector<int> A[N];//list of neighbors
    vector<int> size;
    int* component;
    component=(int*)malloc(N*sizeof(int));
    int color,sizeTemp,max;
    int** W;
    double** kernel;
    
    int nbar; //avg number of transactions
    double wo;
    
    vector<double> wunif(N,0); //wealth from uniform distribution [0,wo]
    vector<double> wexp(N,0); //wealth from exp distribution with mean wo
    vector<int> npoiss(N,0); //number of transactions per node from Poisson distrib
    vector<double> fitness(N,0); //nodes fitness
    vector<double> fitnessexp(N,0); //nodes fitness with exp wealth
    double phi; //blockchain fee rate
    double c; //LN cost
    double nbarmax=50;
    int nloopmax=5;
    vector<double> fitnessmloop(nloopmax,0);
    vector<double> sizeloop(nloopmax,0);
    vector<double> sizeavgnbar(nbarmax,0);
    vector<double> fitnessmloopavg(nbarmax,0);
    vector<double> nplusloop(nloopmax,0);
    vector<double> nplusavg(nbarmax,0);
    
    double mu; //avg number of channels per node
    //int Nplus; // number of nodes with fitness > c/phi
    //double Chat;
    
    
    //   cout << "Please enter an integer value for nbar (avg number of transactions): ";
    //   cin >> nbar;
    cout << "Please enter a value (double) for wo (avg wealth): ";
    cin >> wo;
    
    cout << "Please enter a value (double) for phi rate [0,1] (blockchain cost): ";
    cin >> phi;
    
    cout << "Please enter a value (double) for LN costs: ";
    cin >> c;
    
    cout << "Please enter a value (double) for mu (avg number of channels per node): ";
    cin >> mu;
    

    
    //Wealth generation from uniform distribution
    for(int nbar = 0; nbar < nbarmax; ++nbar){
        for(int nloop = 0; nloop < nloopmax; ++nloop){
        
        
            std::random_device rd;
            std::mt19937 gene(rd());
        
            for(int i = 0; i < N; ++i){
                wunif[i] = randMToN(0.0,wo);
            }
            
            
            //Number of transactions generation from Poisson distribution
            unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
            default_random_engine gen(seed);
            poisson_distribution<int> pdistribution(nbar);
            for (int i = 0; i < N; ++i){
                npoiss[i]=pdistribution(gen);
            }
            //Fitness = wealth * number of transactions

            for (int i = 0; i < N; ++i){
                fitness[i] = (npoiss[i]+1)*(wunif[i]) ;
            }
            
            double fitnessmedia;
            double ftemp=0.0;
            for (int i = 0; i < N; ++i){
                ftemp += fitness[i];
            }
            
            fitnessmedia = ftemp/(double)N;
            cout << fitnessmedia << "fitnessmedia\n";
            fitnessmloop[nloop]=fitnessmedia;
            
//        Find & count high-fitness nodes
            double nplusmedia;
            int Nplus=0;
            for (int i = 0; i < N; ++i){
                if(fitness[i]>c/phi)
                {Nplus+=1;}
            }
            
            nplusmedia = (double)Nplus/N;
            cout << nplusmedia << "nplusmedia\n";
            nplusloop[nloop]=nplusmedia;
            
//        Create adjacency matrix of high-fitness nodes
            double p;
            p= mu/(double)N;
            for(int i=0;i<Nplus;++i)
                A[i].clear();
            for(int i=0;i<Nplus;++i){
                for(int j=i+1;j<Nplus;++j){
                    if(casual()<p){
                        A[i].push_back(j);//adds j to the list of neighbors of i
                        A[j].push_back(i);//adds i to the list of neighbors of j
                    }
                }
            }
            
            // calculate component
            
            for(int i=0;i<N;++i){
                component[i]=0;
            }
            color=1;
            for(int i=0;i<N;++i){
                if(component[i]==0){
                    coloring(i,N,color,component,A);
                    color++;
                }
            }
            
            for(int c=0;c<color;++c){
                sizeTemp=0;
                for(int i=0;i<N;++i){
                    if(component[i]==c)
                        sizeTemp+=1;
                }
                size.push_back(sizeTemp);
            }
            max=size[1];
            for(int i=0;i<size.size();++i){
                if(size[i]>max){
                    max=size[i];
                }
            }
            cout<<fitnessmedia<<" "<<max*1.0/N<<endl; //print fitness media vs size
            size.clear();
            
            sizeloop[nloop] = max*1.0/N;
            
        }//close loop on nloop
        double sizeavg =0.0;
        for(int nloop = 0; nloop < nloopmax; ++nloop){
            sizeavg += sizeloop[nloop];
        }
        
        sizeavgnbar[nbar] = sizeavg/nloopmax;
        
        double fitavg =0.0;
        for(int nloop = 0; nloop < nloopmax; ++nloop){
            fitavg += fitnessmloop[nloop];
        }
        
        fitnessmloopavg[nbar] = fitavg/nloopmax;
        
        double nplusav =0.0;
        for(int nloop = 0; nloop < nloopmax; ++nloop){
            nplusav += nplusloop[nloop];
        }
        
        nplusavg[nbar] = nplusav/nloopmax;
    
    ofstream myfileavg;
    myfileavg.open("unifnplus.txt");
    myfileavg <<"fitnessmedia"<<" "<< "nbar"<<" "<< "size\n" << " "<< "nplus"<<endl;
    for(nbar = 0; nbar < nbarmax; ++nbar){
        myfileavg<<fitnessmloopavg[nbar]<<" "<<nbar <<" "<<sizeavgnbar[nbar]<<" " << nplusavg[nbar]<<"\n"<<endl;
    }
    myfileavg.close();
    return 0;
}

///////////////////// functions ////////////////////////////////


double casual(){
    double r;
    r=rand()/((double)RAND_MAX+1.0);
    return r;
}   //end casual

void coloring(int node, int N, int color, int* component, vector<int> A[]){
    int j;
    component[node]=color;
    for(j=0;j<A[node].size();++j){
        if(component[A[node][j]]==0) {
            coloring(A[node][j],N,color,component,A);
            
        }
    }
}

double** malloc2d_double(int r, int c){
    int i;
    double **t;
    t=(double **)malloc(r*sizeof(double*));
    for(i=0; i<r; i++)
        t[i]=(double *)malloc(c*sizeof(double));
    return t;
}//end malloc2d_double

int** malloc2d_int(int r, int c){
    int i;
    int **t=(int **)malloc(r*sizeof(int*));
    for(i=0; i<r; i++)
        t[i]=(int *)malloc(c*sizeof(int));
    return t;
}//end malloc2d_int

int sign_fun(double x) {
    if (x > 0.0) return 1;
    if (x < 0.0) return 0;
    return 0;
}

double randMToN(double r, double s)
{
    return r + (rand() / ( RAND_MAX / (s-r) ) ) ;
}


